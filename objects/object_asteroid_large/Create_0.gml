/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 59EEEFBB
/// @DnDArgument : "code" "// Don't animate through frames$(13_10)image_speed = 0;$(13_10)$(13_10)// pick a random frame$(13_10)image_index = irandom_range(0, 3);$(13_10)$(13_10)// rotate it in a random position$(13_10)direction = irandom(360);$(13_10)image_angle = irandom(360);$(13_10)$(13_10)speed = 0.5 + random(2);$(13_10)$(13_10)// Set rotation$(13_10)rotate = .5 - random(1);"
// Don't animate through frames
image_speed = 0;

// pick a random frame
image_index = irandom_range(0, 3);

// rotate it in a random position
direction = irandom(360);
image_angle = irandom(360);

speed = 0.5 + random(2);

// Set rotation
rotate = .5 - random(1);