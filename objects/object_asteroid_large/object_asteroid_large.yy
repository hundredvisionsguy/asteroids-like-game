{
    "id": "6ec59d12-bf0c-4dd3-b6a6-15cc476976cd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_asteroid_large",
    "eventList": [
        {
            "id": "1437a9e7-61f1-4fdf-b55f-13756c89ce58",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6ec59d12-bf0c-4dd3-b6a6-15cc476976cd"
        },
        {
            "id": "426a2978-776c-410f-8ecd-5e71962b6328",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "6ec59d12-bf0c-4dd3-b6a6-15cc476976cd"
        },
        {
            "id": "68af883f-55e3-4af4-ad2e-f53f6a148df0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6ec59d12-bf0c-4dd3-b6a6-15cc476976cd"
        },
        {
            "id": "358ed549-30fd-4a9e-9dbe-141bdfb0d5b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "7aad15d9-447a-471f-9f6f-c3cb5bb7dc57",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6ec59d12-bf0c-4dd3-b6a6-15cc476976cd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cef47afe-ed15-428d-91b0-e8e2831722e0",
    "visible": true
}