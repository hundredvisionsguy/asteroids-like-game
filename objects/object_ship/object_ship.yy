{
    "id": "ea3fed8a-b742-4632-aa26-f9f86724e621",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_ship",
    "eventList": [
        {
            "id": "2453cce3-30a9-44d9-a805-93d158681dc9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 37,
            "eventtype": 5,
            "m_owner": "ea3fed8a-b742-4632-aa26-f9f86724e621"
        },
        {
            "id": "0999206a-99f8-41a1-96da-3ab17e1c5144",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 39,
            "eventtype": 5,
            "m_owner": "ea3fed8a-b742-4632-aa26-f9f86724e621"
        },
        {
            "id": "f77f77c3-f3e2-45ba-b4d7-f41290c9db14",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 38,
            "eventtype": 5,
            "m_owner": "ea3fed8a-b742-4632-aa26-f9f86724e621"
        },
        {
            "id": "daf9540f-7ca4-43dc-9c57-3338251a92f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ea3fed8a-b742-4632-aa26-f9f86724e621"
        },
        {
            "id": "9155ed70-fe9e-436c-9d1f-3f5beb281d41",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "ea3fed8a-b742-4632-aa26-f9f86724e621"
        },
        {
            "id": "6a8bea5f-12e7-4093-986c-8cb7eaa253f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "ea3fed8a-b742-4632-aa26-f9f86724e621"
        },
        {
            "id": "b6fadd57-4fe0-4b8c-9778-42e7f9be71b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "6ec59d12-bf0c-4dd3-b6a6-15cc476976cd",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ea3fed8a-b742-4632-aa26-f9f86724e621"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1718af20-fb4a-4f3b-92d4-7c6990abf819",
    "visible": true
}