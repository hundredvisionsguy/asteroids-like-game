/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 673036D1
/// @DnDArgument : "expr" "-1"
/// @DnDArgument : "expr_relative" "1"
/// @DnDArgument : "var" "object_game_controller.ship1_lives"
object_game_controller.ship1_lives += -1;

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 5A394EEA
/// @DnDArgument : "var" "object_game_controller.ship1_lives"
/// @DnDArgument : "op" "2"
if(object_game_controller.ship1_lives > 0)
{
	/// @DnDAction : YoYo Games.Rooms.Restart_Room
	/// @DnDVersion : 1
	/// @DnDHash : 4C1EAC21
	/// @DnDParent : 5A394EEA
	room_restart();
}