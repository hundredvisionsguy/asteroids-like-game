{
    "id": "044f2902-ef90-4e42-a131-b8286d499c2e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_asteroid_small",
    "eventList": [
        {
            "id": "6d4ac283-acb8-418a-8ae2-6431e9809362",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "7aad15d9-447a-471f-9f6f-c3cb5bb7dc57",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "044f2902-ef90-4e42-a131-b8286d499c2e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b46844a-d7d4-461f-bc85-0d6dfa124fb6",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2315183e-d981-43a6-a0f9-658a0344a180",
    "visible": true
}