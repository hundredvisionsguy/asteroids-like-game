/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 4793DCFA
/// @DnDApplyTo : b1a1b115-8622-409d-959d-4993f171d60b
/// @DnDArgument : "expr" "100"
/// @DnDArgument : "expr_relative" "1"
/// @DnDArgument : "var" "object_game_controller.ship1_score"
with(object_game_controller) {
object_game_controller.ship1_score += 100;

}

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 3DD35838
instance_destroy();