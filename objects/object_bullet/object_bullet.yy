{
    "id": "7aad15d9-447a-471f-9f6f-c3cb5bb7dc57",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_bullet",
    "eventList": [
        {
            "id": "2ffb4de1-2ff0-4827-8500-a6e50c25bd17",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "6ec59d12-bf0c-4dd3-b6a6-15cc476976cd",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7aad15d9-447a-471f-9f6f-c3cb5bb7dc57"
        },
        {
            "id": "350f9bc8-8686-47e2-898d-19e6fdf6851b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "7aad15d9-447a-471f-9f6f-c3cb5bb7dc57"
        },
        {
            "id": "11e56e6a-cbf2-4f35-9556-d5693595d239",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "4b46844a-d7d4-461f-bc85-0d6dfa124fb6",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7aad15d9-447a-471f-9f6f-c3cb5bb7dc57"
        },
        {
            "id": "0b9b2607-d1a3-42ad-b2ae-514ffe2d598c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "044f2902-ef90-4e42-a131-b8286d499c2e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7aad15d9-447a-471f-9f6f-c3cb5bb7dc57"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "248f457f-a1cf-4d73-9df1-1b9ebcb8a752",
    "visible": true
}