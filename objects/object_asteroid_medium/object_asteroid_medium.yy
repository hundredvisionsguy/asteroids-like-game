{
    "id": "4b46844a-d7d4-461f-bc85-0d6dfa124fb6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_asteroid_medium",
    "eventList": [
        {
            "id": "06112d02-eddb-4c7f-844c-bcdaee4c582b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "7aad15d9-447a-471f-9f6f-c3cb5bb7dc57",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "4b46844a-d7d4-461f-bc85-0d6dfa124fb6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6ec59d12-bf0c-4dd3-b6a6-15cc476976cd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1d32fd5f-7d24-419e-a4b3-867a73b4b0e1",
    "visible": true
}