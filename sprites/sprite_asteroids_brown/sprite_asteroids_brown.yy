{
    "id": "cef47afe-ed15-428d-91b0-e8e2831722e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_asteroids_brown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 97,
    "bbox_left": 0,
    "bbox_right": 119,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fd450cf9-9fda-4caf-b29d-7b11bb97f6dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cef47afe-ed15-428d-91b0-e8e2831722e0",
            "compositeImage": {
                "id": "7a372cfc-7841-4c61-84ce-6c4ed021f91b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd450cf9-9fda-4caf-b29d-7b11bb97f6dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1b218e4-98ce-4b52-9075-de1d3fda6140",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd450cf9-9fda-4caf-b29d-7b11bb97f6dd",
                    "LayerId": "88e7f4bb-7902-4c72-835a-83c32d6e1114"
                }
            ]
        },
        {
            "id": "9abaa5a0-9c70-4fa2-b081-2e5deb78b668",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cef47afe-ed15-428d-91b0-e8e2831722e0",
            "compositeImage": {
                "id": "a979c87b-7abc-4913-a08a-fb8a3a42d6c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9abaa5a0-9c70-4fa2-b081-2e5deb78b668",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef6ffcc8-966a-49dd-83d4-8f8a3d28ad3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9abaa5a0-9c70-4fa2-b081-2e5deb78b668",
                    "LayerId": "88e7f4bb-7902-4c72-835a-83c32d6e1114"
                }
            ]
        },
        {
            "id": "433b391e-923c-42af-a22f-628461c3686d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cef47afe-ed15-428d-91b0-e8e2831722e0",
            "compositeImage": {
                "id": "0f3612be-ec2d-4226-b795-f88df9d6158c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "433b391e-923c-42af-a22f-628461c3686d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62d41a89-38f2-45f2-927d-21bfefcf4487",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "433b391e-923c-42af-a22f-628461c3686d",
                    "LayerId": "88e7f4bb-7902-4c72-835a-83c32d6e1114"
                }
            ]
        },
        {
            "id": "4ae4afb7-9d7b-4f59-b541-0220fb11122b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cef47afe-ed15-428d-91b0-e8e2831722e0",
            "compositeImage": {
                "id": "68790c76-b714-4b05-ac65-b4c98943934b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ae4afb7-9d7b-4f59-b541-0220fb11122b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f266563-68e7-4a3f-9c32-06a6cf16c48b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ae4afb7-9d7b-4f59-b541-0220fb11122b",
                    "LayerId": "88e7f4bb-7902-4c72-835a-83c32d6e1114"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 98,
    "layers": [
        {
            "id": "88e7f4bb-7902-4c72-835a-83c32d6e1114",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cef47afe-ed15-428d-91b0-e8e2831722e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 120,
    "xorig": 60,
    "yorig": 49
}