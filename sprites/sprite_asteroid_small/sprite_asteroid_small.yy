{
    "id": "2315183e-d981-43a6-a0f9-658a0344a180",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_asteroid_small",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 28,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "120be7e9-7b0d-4c30-a3ea-b73f6862cf8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2315183e-d981-43a6-a0f9-658a0344a180",
            "compositeImage": {
                "id": "28b09da0-d4a4-4831-b66d-6277d60e32de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "120be7e9-7b0d-4c30-a3ea-b73f6862cf8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc00e471-a40a-40dd-b352-313155bad2b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "120be7e9-7b0d-4c30-a3ea-b73f6862cf8a",
                    "LayerId": "15af1a48-23d6-46c1-abde-e3b320bcf6a0"
                }
            ]
        },
        {
            "id": "b544be8a-2d8a-48fc-855c-82d5b6a6a49b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2315183e-d981-43a6-a0f9-658a0344a180",
            "compositeImage": {
                "id": "fab4daa2-2dc0-4962-9c45-86a631859dd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b544be8a-2d8a-48fc-855c-82d5b6a6a49b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3c64f93-4a24-4ec4-bb5e-a4b0b53b9eba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b544be8a-2d8a-48fc-855c-82d5b6a6a49b",
                    "LayerId": "15af1a48-23d6-46c1-abde-e3b320bcf6a0"
                }
            ]
        },
        {
            "id": "9cc2c9a4-138f-4a26-bf46-4180d27aa3d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2315183e-d981-43a6-a0f9-658a0344a180",
            "compositeImage": {
                "id": "9f5d0b1d-887f-4cc8-b38e-2dd596e00de1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cc2c9a4-138f-4a26-bf46-4180d27aa3d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b4ab916-2f3f-4f4b-9374-95030754e510",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cc2c9a4-138f-4a26-bf46-4180d27aa3d4",
                    "LayerId": "15af1a48-23d6-46c1-abde-e3b320bcf6a0"
                }
            ]
        },
        {
            "id": "1fce1825-838f-4196-a127-caf9f7a46204",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2315183e-d981-43a6-a0f9-658a0344a180",
            "compositeImage": {
                "id": "452efb2b-5076-4063-8398-a50ade4ef6f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fce1825-838f-4196-a127-caf9f7a46204",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d2da43c-9453-4c0e-9f27-cddd5d62f012",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fce1825-838f-4196-a127-caf9f7a46204",
                    "LayerId": "15af1a48-23d6-46c1-abde-e3b320bcf6a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "15af1a48-23d6-46c1-abde-e3b320bcf6a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2315183e-d981-43a6-a0f9-658a0344a180",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 29,
    "xorig": 14,
    "yorig": 14
}