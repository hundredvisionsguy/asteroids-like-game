{
    "id": "1d32fd5f-7d24-419e-a4b3-867a73b4b0e1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_asteroid_medium",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 42,
    "bbox_left": 0,
    "bbox_right": 44,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c6c09f5-551f-4762-9e9f-a592f447add3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d32fd5f-7d24-419e-a4b3-867a73b4b0e1",
            "compositeImage": {
                "id": "49bd52ea-ccb1-4cce-9ccd-ccd97943b377",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c6c09f5-551f-4762-9e9f-a592f447add3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc067304-4854-4493-930d-0ee21e6638c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c6c09f5-551f-4762-9e9f-a592f447add3",
                    "LayerId": "288c3bd2-d1bc-4135-97e4-4ce646578823"
                }
            ]
        },
        {
            "id": "5c59bfa9-385f-4dbf-a101-06fd6ad986f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d32fd5f-7d24-419e-a4b3-867a73b4b0e1",
            "compositeImage": {
                "id": "33d51c51-1bd9-4d8b-a3de-e57e075cd922",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c59bfa9-385f-4dbf-a101-06fd6ad986f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f79576e0-1b7e-4806-9d78-ddad62b00138",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c59bfa9-385f-4dbf-a101-06fd6ad986f4",
                    "LayerId": "288c3bd2-d1bc-4135-97e4-4ce646578823"
                }
            ]
        },
        {
            "id": "2883061d-301c-4ca0-be74-ec5cfc06ac86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d32fd5f-7d24-419e-a4b3-867a73b4b0e1",
            "compositeImage": {
                "id": "0b648373-29ae-4f7e-bbae-f4a56871c318",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2883061d-301c-4ca0-be74-ec5cfc06ac86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ed68ca6-df0f-441b-902f-620cafce6eaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2883061d-301c-4ca0-be74-ec5cfc06ac86",
                    "LayerId": "288c3bd2-d1bc-4135-97e4-4ce646578823"
                }
            ]
        },
        {
            "id": "3d5c5e23-e9d9-46fc-84dc-b97cefa9b492",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d32fd5f-7d24-419e-a4b3-867a73b4b0e1",
            "compositeImage": {
                "id": "5b3f4de9-c98f-48e4-a83c-4b94affaad5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d5c5e23-e9d9-46fc-84dc-b97cefa9b492",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4890e393-cc5c-4a45-98e3-ba87100d9417",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d5c5e23-e9d9-46fc-84dc-b97cefa9b492",
                    "LayerId": "288c3bd2-d1bc-4135-97e4-4ce646578823"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 43,
    "layers": [
        {
            "id": "288c3bd2-d1bc-4135-97e4-4ce646578823",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d32fd5f-7d24-419e-a4b3-867a73b4b0e1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 45,
    "xorig": 22,
    "yorig": 21
}