{
    "id": "352b0718-1af3-492d-ba80-70c7a6e53aee",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_hud",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Lucida Console",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "5ca35830-affd-42dc-b4a8-6635dd9e5d27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 18,
                "y": 28
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "928a9374-7de9-4c82-8479-1c9a793222e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 24,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 29,
                "y": 132
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "989e3969-2545-4aa3-b0cb-d5e52f6d43ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 142,
                "y": 106
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "e695307e-f6cb-470e-af86-707a2156b370",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 50,
                "y": 28
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "fd35ca38-7885-47ab-8f31-7b22c87a205b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 54,
                "y": 106
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "fa64bc7e-5d04-48ec-a7b2-958c760f3371",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 155,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "22057dbd-4720-44b9-b659-2834eb93aea7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 138,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "5276e7cb-82a4-4855-9825-103d205e48be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 24,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 35,
                "y": 132
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "ea733f11-6090-4b8e-8f7c-695b1a9443f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 24,
                "offset": 4,
                "shift": 14,
                "w": 9,
                "x": 201,
                "y": 106
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "aa4527a1-5e3d-4272-bce2-6abac0e53f81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 10,
                "x": 166,
                "y": 106
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "852e4f27-dd8a-4ebc-8b8a-1161b49e48a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 166,
                "y": 54
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "09aaa300-b9b0-4b89-bcb5-499728441765",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 157,
                "y": 28
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "286e9e95-bcac-46b9-8043-c0202de9f12d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 24,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 11,
                "y": 132
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "3b75c901-cee4-40ea-b10b-914bb7f3765e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 178,
                "y": 106
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "324c1e23-d86e-4d76-90a5-db5fc0c7cc39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 24,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 17,
                "y": 132
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "720544d4-d619-4e98-ab0f-f4cef1c92d13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 66,
                "y": 28
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "43c0682b-1b5b-40de-bec9-97ffccdce3bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 32,
                "y": 54
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "2901dd6e-7613-42f0-a78c-aea9182a8a75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 180,
                "y": 54
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "f98e1ff0-81c4-4781-b64d-1c9c02e33342",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 41,
                "y": 106
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "c8eee1c6-8187-4889-b7c5-d2a4de50ae82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 67,
                "y": 106
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "e6e7843e-b036-4cf1-b3f9-e1a490288108",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 77,
                "y": 54
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "9600455c-e4df-4b24-8dda-00d9312c9b8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 24,
                "offset": 3,
                "shift": 14,
                "w": 10,
                "x": 130,
                "y": 106
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "6e922b56-b432-4237-be7a-1aec4fc16524",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 137,
                "y": 54
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "103f8443-a283-4c78-bbdf-08b3ee74aab0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 80,
                "y": 106
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "9ee6ffe8-ccd6-4729-9cd3-f7895241da78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 152,
                "y": 54
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "01525eb6-7774-4d12-8bc0-ad58715cdf4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 156,
                "y": 80
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "2c7d7a64-3c87-4173-9f55-31279723fa41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 24,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 23,
                "y": 132
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "b24ddd86-b544-4372-a05e-a0558b071795",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 24,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 41,
                "y": 132
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "15fef225-148b-409a-8705-cb175cbeb47c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 107,
                "y": 54
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "4f4d402e-31d1-4259-9511-2f33d1ed2f9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 62,
                "y": 54
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "5c79c2ab-4a2f-4680-a34a-7be8cc487fd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 47,
                "y": 54
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "dbb0c647-5837-45bb-8982-3680045cab7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 30,
                "y": 80
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "fbf5e30f-376b-42f4-8f8c-770a7a72feb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 121,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "3d8fd7fb-3574-4243-9ea1-90fefc4b1c8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "6c82c56a-6f6d-4330-b5dd-a8f8bb0836b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 170,
                "y": 80
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "55128e2b-5d87-4c66-8d02-de1bc946ea5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 2,
                "y": 54
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "f427f159-1336-4a4c-8659-ba705d5b3d69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 232,
                "y": 28
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "8ce316a4-69e9-4096-91c5-a1b9e769b9ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 198,
                "y": 80
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "4ffe2900-fb51-465e-a8e6-d8c6e3698e7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 212,
                "y": 80
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "54efb91b-c345-4cdd-91d0-e51179b5307e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 238,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "121a1cee-b286-4b04-ac0f-2071b7c8e065",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 184,
                "y": 80
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "36cfddb8-41ad-462f-b69c-e0c479a3f5df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 93,
                "y": 106
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "85ba2bd7-e35f-4bd3-8080-5ac51b046094",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 9,
                "x": 190,
                "y": 106
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "c13c67bf-d167-4ab2-b08f-85e65f71d3f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 13,
                "x": 17,
                "y": 54
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "eaa844ba-34bb-42cd-a99c-58179bb751dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 114,
                "y": 80
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "16c8f823-e6c7-4313-85ae-0fbe9b6f643e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 217,
                "y": 28
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "9a434566-8f25-48f3-9128-8ebdf067263a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 100,
                "y": 80
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "75758f89-5bae-4b6c-b370-b92be127aaab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 34,
                "y": 28
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "54d739b7-8037-4c76-ac6d-aba81dc9734b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 72,
                "y": 80
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "815dbe4a-2782-40ec-9741-80c32fc560dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 19,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "d65136da-0b1d-4205-9843-5f1a4f78ff65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 13,
                "x": 187,
                "y": 28
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "be6d1cf8-ca39-425e-a408-527133d3dddb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 172,
                "y": 28
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "f5d69951-9898-4eec-93a0-e09c94d18846",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "1c4cc25a-81a7-4896-8b25-d35bbce34656",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 208,
                "y": 54
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "f688519d-dac5-402d-bad4-36c14105397d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "91365bda-85c9-48b0-a810-68ff29455840",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 70,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "cabe0fcc-b647-492d-ab83-6b041353b706",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "0416082b-5649-4d88-8fec-2baf08777f42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "7ac1ff88-5c32-4f7e-9a3d-3b3318b4e591",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 127,
                "y": 28
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "928019ee-060c-4332-8ddb-003de8b1f65f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 24,
                "offset": 5,
                "shift": 14,
                "w": 8,
                "x": 242,
                "y": 106
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "07199b99-d6dd-4c74-88a5-58c400eccc58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 28
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "f41509ac-14ff-4ad9-8684-0da633ea52ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 8,
                "x": 232,
                "y": 106
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "fd175d83-7c1f-45df-a85a-6596cea9c150",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 112,
                "y": 28
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "8ef29522-5b9c-43f8-a9e6-5b6b4498fd6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 172,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "aedf6b15-0fec-444f-a68f-945c6ee87b88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 24,
                "offset": 4,
                "shift": 14,
                "w": 7,
                "x": 2,
                "y": 132
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "4b8b8c72-4593-4353-999a-823c925ff453",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 97,
                "y": 28
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "26a0f2fd-efdd-4d17-80c2-e4e7be857a03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 80
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "29ef826d-e5c8-4512-8500-d6f737535332",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 128,
                "y": 80
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "645869be-3f79-4d1b-8cbc-99ef92015f9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 44,
                "y": 80
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "3d727bc6-0358-4c72-b2c3-0dcfd635b4d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 222,
                "y": 54
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "ede0268e-af9c-494e-83a7-feb6253b3872",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 14,
                "x": 206,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "4b15552a-7c1e-4af6-a4f1-2a682e5bb1c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 194,
                "y": 54
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "97760e7a-238e-4a34-992c-107705c96d24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 239,
                "y": 80
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "8b0d4b4f-877e-4851-b8ce-41ce5a971a74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 8,
                "x": 212,
                "y": 106
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "09d3d3c1-e2c7-4eb8-8f24-1a58b91870b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 10,
                "x": 106,
                "y": 106
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "687bacde-19fc-446c-b124-922bf949c407",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 16,
                "y": 80
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "f05001a2-838e-4b56-ae5a-71b9a0f4d4fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 8,
                "x": 222,
                "y": 106
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "6edc23f7-f23c-4876-9396-463a3385a070",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 142,
                "y": 28
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "9cc96423-481a-46c2-9da6-c724b2c12401",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 226,
                "y": 80
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "de107387-5779-4485-b95e-4fafc9aa430d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 202,
                "y": 28
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "8998baf3-d353-40f6-80a9-eaf778f35dc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 86,
                "y": 80
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "dd339140-220e-400d-bb7d-dbe67466deb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 142,
                "y": 80
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "eb04721a-9686-428a-afad-8c36e926580b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 24,
                "offset": 3,
                "shift": 14,
                "w": 11,
                "x": 2,
                "y": 106
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "21f4819b-39df-4e83-b958-b2641af36cb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 15,
                "y": 106
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "546c075d-4844-44e7-92d6-451ce1638e12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 58,
                "y": 80
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "eac41cfc-fdb4-43b9-8125-d69ced8bb0f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 28,
                "y": 106
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "62a4423c-27b5-438e-aa27-cb8aa3bdc15a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "54afd5ba-f731-4d3a-b8cf-4ba4ab36cabe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 189,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "b609ede6-e084-4c1d-9937-401d9ce3cb5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 122,
                "y": 54
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "90765198-795f-4a43-9559-a609d269ba7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 92,
                "y": 54
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "386cd786-ad97-41e2-94ab-085593559306",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 236,
                "y": 54
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "bb559780-a2ec-42e3-b708-4bac7b54d710",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 118,
                "y": 106
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "a2336995-18c2-41c9-98e0-138f4367e514",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 24,
                "offset": 6,
                "shift": 14,
                "w": 3,
                "x": 47,
                "y": 132
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "3bd12ac4-ac07-4ca2-ad6c-cb7df32e7caa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 154,
                "y": 106
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "7a61f757-4756-466d-a93b-5f67cdc42531",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 82,
                "y": 28
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 18,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}